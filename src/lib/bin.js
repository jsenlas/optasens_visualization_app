function draw() {
    let dataRow;
    let posX;
    let posY;
    
    // let data = $data_store.slice(-$last_row_num);
    let row_number = ($data_store.length < $chart_row_num) ? $data_store.length : $chart_row_num;
    // if (data.length < $last_row_num){
    //     row_number = data.length
    // } else {
    //     row_number =  $last_row_num; // ($data_store.length < $chart_row_num) ? $data_store.length : $chart_row_num;
    // }
    // console.log("rownumbr", row_number);
    // console.log("DATA", data);
    for (let y = 0; y < row_number; y++){
        dataRow = $data_store[y];
        posY = y * $cellHeight;
        for (let x = 0; x < dataRow.length; x++){
            posX = x * $cellWidth;
            ctx.fillStyle = getFill(dataRow[x]);
            ctx.fillRect(
                posX,
                posY, 
                posX + $cellWidth,
                $cellHeight
            );
        }
    }
}