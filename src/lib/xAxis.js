import { get } from 'svelte/store';
import {
    channel_number
} from './canvas_properties'
import * as d3 from 'd3';

let singleton_flag = true;

function getTicks(){
    let cn = get(channel_number)
    if (cn > 1000){
        return cn/100;    
    } else if (cn >= 100) {
        return cn/5;
    } else {
        return cn;
    }
    
}

export function displayXaxis() {
    if (singleton_flag) {
        singleton_flag = false;
        let bbb = document.getElementById("chart_border");
        let chart = d3.select("#xAxis")
                .append("svg")
                .attr("width", bbb.clientWidth)//get(width) + 50 + "px")//$width) //15 means more space for the number 100 at the end
                .style("font-size","50px")
                .attr("height", 20);
    
        let scale = d3.scaleLinear()
                .domain([0, get(channel_number)])
                .range([0, bbb.clientWidth-10]);
        let x_axis = d3.axisBottom()
                    .scale(scale)
                    .ticks(getTicks())
                    
        chart.append("g")
            .call(x_axis)
            .attr("transform", "translate(" + 5 + ",0)");
    }
}