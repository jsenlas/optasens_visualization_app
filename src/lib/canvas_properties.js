import { writable, get, derived } from 'svelte/store';
import * as d3 from 'd3';

export const width = writable(100);    // canvas W
export const height = writable(100);   // canvas H
export const channel_number = writable(100);  // number of channels
// width of cells in the heatmap
export const cellWidth = writable(10);
// export const cellWidth = derived(
//     [ width, channel_number ],
//     ([ $width, $channel_number ], set) => {
//         set($width/$channel_number)
//     }
// );

export const cellHeight = writable(10);    // height of a cell in px
export const chart_row_num = writable(100); // MAX number of rows to render in the canvas
export const last_row_num = writable(0);    // 
export const dataset_rows = writable(0);    // 
export const dataset_min = writable(0);     // min value from the dataset
export const dataset_max = writable(1000);  // max value from the dataset
export const colormap = writable(d3.interpolateTurbo);
export const speed = writable(0);
export const intervalID = writable(setInterval(()=>{}, 1000000));
export const frameID = writable(0);

export const fps = derived(
    [ speed ],
    ([ $speed ], set) => {
        set(fpsScale($speed))
    }
);
export const interval = derived(
    [ fps ],
    ( [ $fps ], set ) => {
        set(1000/$fps)
    }
 )

let idx = 1;
export function increment(){
    idx++;
    last_row_num.set(idx);
}

let speedScale = d3.scaleLinear().domain([100, 0]).range([0, 1000]);
let fpsScale = d3.scaleLinear().domain([100, 0]).range([0, 60]);
export function countDelay(){
    // speed is 0-100
    console.log("SPEEED", speedScale(get(speed)))
    return speedScale(get(speed));
}

export function setFPS(){
    return fpsScale(get(speed));
}

export function download_image() {
    const image = new Image();
    let canvas = document.getElementById("mycanvas");
    image.src = canvas.toDataURL("image/png");

    // Create a download link and click it
    const link = document.createElement("a");
    link.download = "canvas_image.png";
    link.href = image.src;
    link.click();
}

export function clear_canvas() {
    let canvas = document.getElementById("mycanvas");
    let ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, get(width), get(height));
}
let colorScale = d3.scaleLinear()
                        .domain([0, 1000]) //([$dataset_min, $dataset_max])
                        .range([0,1]);

export function set_colorScaleStore(a, b, c, d) {
    console.log(a, b);
    colorScaleStore.set(
        d3.scaleLinear()
            .domain([c, d])
            .range([a,b])
    )
}

export const colorScaleStore = writable(colorScale);
export const startColor = writable();
export const endColor = writable();
export const lowBoundary = writable(0);
export const highBoundary = writable(1000);
